#
# Grafana / Graphite related functionalities.
#

import os
import time
import yaml
import socket

from typing import Dict, List, Optional
from dataclasses import dataclass


@dataclass
class GraphiteEntry():
    """
    Object representing a Graphite entry for a single hardware entry.
    This class holds per-HW information that will be pushed to Graphite.

    serial      : Serial number of the hardware
    """
    serial: str

    def __post_init__(self):
        # Initialize the map of attributes for this HW entry
        # -> { name: value }
        self.attributes = {}


    def set_attribute(self, attribute_name: str, value: int):
        """
        Set an attribute of this hardware entry.
        """
        self.attributes[attribute_name] = value


    def convert_to_graphite_strings(self, identifier: str) -> List[str]:
        """
        Converts this entry into Graphite-recognizable strings and returns it.

        'identifier' parameter will determine the first two parameters of the Graphite name.
        i.e. "A.B" in the full name of "A.B.C.D"
        """
        timestamp = int(time.time())

        # Padded serial number of the HW in hex format
        serial_hex = str.format('0x{:02X}', int(self.serial))

        # Each attribute of this HW will be turned into a Graphite string
        result = []
        for aname, avalue in self.attributes.items():
            result.append(f"{identifier}.{serial_hex}.{aname} {avalue} {timestamp}\n")
        
        return result


class GraphiteHelper():
    """
    Helper object to push data to Graphite server.

    Initialize this object with the app configuration extracted from the 
    YAML configuration file, having information related to Graphite settings.

    We expect YAML configuration for Graphite to be in the following format:
    
    graphite:
        push: true/false
        server:
            ip: <ip_addr>
            port: <port_num>

    If fields are missing from the YAML configuration file, this object
    will raise a RuntimeError during initialization.
    """
    def __init__(self, config: dict) -> None:
        # Extract settings out of the configuration
        if "graphite" not in config:
            raise RuntimeError("'graphite' top-level key not found in config")

        self.settings = config["graphite"]

        # Set the boolean flag specifies whether we want to push 
        # to Graphite server
        try:
            self.do_push = self.settings["push"]
        
        # Missing "push" field from the config
        except KeyError:
            raise RuntimeError("push: true/false not specified in the configuration")


    def push_to_graphite(self, entries: List[GraphiteEntry]) -> None:
        """
        Push the given data into the Graphite server.
        The given data should be a list of GraphiteEntry instances.
        """
        # If the Graphite-push is disabled by the user, do nothing
        if not self.do_push:
            return

        # Retrieve server information, if we can't find info on this
        # inform the user and do nothing
        try:
            server = self.settings["server"]
        except KeyError:
            print("Graphite server information is missing from configuration")
            print("Will not push to Graphite DB")
            return

        # Open up a socket connection to Graphite server and send the message
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            try:
                s.connect((server["ip"], server["port"]))
            
                # Loop over all the Graphite entries we have (per IPMC),
                # stringify them and send them through the socket.
                for entry in entries:
                    # Turn the entry into graphite recognizable strings
                    strings = entry.convert_to_graphite_strings(self.settings["identifier"])

                    # Encode the data with utf-8 and send it to the socket
                    for string in strings:
                        s.sendall(bytes(string, 'utf-8'))

            # Provided IP address cannot be found
            # Warn the user that we cannot push to Graphite
            except socket.error:
                print(f'Cannot reach Graphite server at IP: {server["ip"]}, port: {server["port"]}')
