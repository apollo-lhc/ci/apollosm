import json_writer
import json



{
  "annotations": {
    "enable": false,
    "list": [
      {
        "builtIn": 1,
        "datasource": {
          "type": "datasource",
          "uid": "grafana"
        },
        "enable": true,
        "hide": true,
        "iconColor": "rgba(0, 211, 255, 1)",
        "name": "Annotations & Alerts",
        "target": {
          "limit": 100,
          "matchAny": false,
          "tags": [],
          "type": "dashboard"
        },
        "type": "dashboard"
      }
    ]
  },
  "editable": true,
  "fiscalYearStartMonth": 0,
  "graphTooltip": 0,
  "id": 1,
  "links": [],
  "liveNow": false,
  "panels": [
    {
      "collapsed": false,
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "gridPos": {
        "h": 1,
        "w": 24,
        "x": 0,
        "y": 0
      },
      "id": 30,
      "panels": [],
      "targets": [
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "refId": "A"
        }
      ],
      "title": "Schroff Shelf",
      "type": "row"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical 13",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x9A"
                },
                "1": {
                  "index": 1,
                  "text": "0x9A"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 3,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 62,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "Shelf.SCHROFF.FRU.0x9a.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.144.IPMC.SN.ID"
        }
      ],
      "timeFrom": "2m",
      "title": "1",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 11",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x96"
                },
                "1": {
                  "index": 1,
                  "text": "0x96"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 4,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 91,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "Shelf.SCHROFF.FRU.0x96.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.150.IPMC.SN.ID"
        }
      ],
      "timeFrom": "2m",
      "title": "2",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 9",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x92"
                },
                "1": {
                  "index": 1,
                  "text": "0x92"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 5,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 90,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "Shelf.SCHROFF.FRU.0x92.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.146.IPMC.SN.ID"
        }
      ],
      "timeFrom": "2m",
      "title": "3",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 7",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x8e"
                },
                "1": {
                  "index": 1,
                  "text": "0x8e"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 6,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 92,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "Shelf.SCHROFF.FRU.0x8e.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.142.IPMC.SN.ID"
        }
      ],
      "timeFrom": "2m",
      "title": "4",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 5",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x8a"
                },
                "1": {
                  "index": 1,
                  "text": "0x8a"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 7,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 93,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "Shelf.SCHROFF.FRU.0x8a.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.138.IPMC.SN.ID"
        }
      ],
      "timeFrom": "2m",
      "title": "5",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 3",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x86"
                },
                "1": {
                  "index": 1,
                  "text": "0x86"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 8,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 94,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "Shelf.SCHROFF.FRU.0x86.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.134.IPMC.SN.ID"
        }
      ],
      "timeFrom": "2m",
      "title": "6",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 1",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x82"
                },
                "1": {
                  "index": 1,
                  "text": "0x82"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 9,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 97,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "Shelf.SCHROFF.FRU.0x82.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.130.IPMC.SN.ID"
        }
      ],
      "timeFrom": "2m",
      "title": "7",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 2",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x84"
                },
                "1": {
                  "index": 1,
                  "text": "0x84"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 10,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 98,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "Shelf.SCHROFF.FRU.0x84.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.132.IPMC.SN.ID",
          "textEditor": true
        }
      ],
      "timeFrom": "2m",
      "title": "8",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 4",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x88"
                },
                "1": {
                  "index": 1,
                  "text": "0x88"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 11,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 99,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "Shelf.SCHROFF.FRU.0x88.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.136.IPMC.SN.ID",
          "textEditor": true
        }
      ],
      "timeFrom": "2m",
      "title": "9",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 6",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x8c"
                },
                "1": {
                  "index": 1,
                  "text": "0x8c"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 12,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 95,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "Shelf.SCHROFF.FRU.0x8c.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.140.IPMC.SN.ID"
        }
      ],
      "timeFrom": "2m",
      "title": "10",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 6",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x90"
                },
                "1": {
                  "index": 1,
                  "text": "0x90"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 13,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 96,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "Shelf.SCHROFF.FRU.0x90.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.144.IPMC.SN.ID"
        }
      ],
      "timeFrom": "2m",
      "title": "11",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 10",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x94"
                },
                "1": {
                  "index": 0,
                  "text": "0x94"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 14,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 72,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "aliasByNode(Shelf.SCHROFF.FRU.0x94.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.148.IPMC.SN.ID"
        }
      ],
      "timeFrom": "2m",
      "title": "12",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 12",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x98"
                },
                "1": {
                  "index": 1,
                  "text": "0x98"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 15,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 100,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "Shelf.SCHROFF.FRU.0x98.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.152.IPMC.SN.ID",
          "textEditor": true
        }
      ],
      "timeFrom": "2m",
      "title": "13",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 14",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x9C"
                },
                "1": {
                  "index": 1,
                  "text": "0x9C"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 4,
        "w": 1,
        "x": 16,
        "y": 1
      },
      "hideTimeOverride": true,
      "id": 101,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "none",
        "justifyMode": "center",
        "orientation": "horizontal",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "value"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refCount": 0,
          "refId": "A",
          "target": "Shelf.SCHROFF.FRU.0x9c.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        },
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "hide": false,
          "refCount": 0,
          "refId": "B",
          "target": "Shelf.Slot.156.IPMC.SN.ID",
          "textEditor": true
        }
      ],
      "timeFrom": "2m",
      "title": "14",
      "type": "stat"
    },
    {
      "alert": {
        "alertRuleTags": {},
        "conditions": [
          {
            "evaluator": {
              "params": [
                45
              ],
              "type": "gt"
            },
            "operator": {
              "type": "and"
            },
            "query": {
              "params": [
                "A",
                "5m",
                "now"
              ]
            },
            "reducer": {
              "params": [],
              "type": "max"
            },
            "type": "query"
          }
        ],
        "executionErrorState": "alerting",
        "for": "0m",
        "frequency": "30s",
        "handler": 1,
        "message": "Schroff over temp",
        "name": "Schroff Fan Tray Temperatures alert",
        "noDataState": "no_data",
        "notifications": [
          {
            "uid": "h_lQ1km7z"
          }
        ]
      },
      "aliasColors": {},
      "bars": false,
      "dashLength": 10,
      "dashes": false,
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "fieldConfig": {
        "defaults": {
          "links": []
        },
        "overrides": []
      },
      "fill": 1,
      "fillGradient": 0,
      "gridPos": {
        "h": 9,
        "w": 10,
        "x": 0,
        "y": 5
      },
      "hiddenSeries": false,
      "id": 3,
      "legend": {
        "alignAsTable": false,
        "avg": false,
        "current": false,
        "max": false,
        "min": false,
        "rightSide": false,
        "show": true,
        "sideWidth": 100,
        "total": false,
        "values": false
      },
      "lines": true,
      "linewidth": 1,
      "nullPointMode": "null",
      "options": {
        "alertThreshold": true
      },
      "percentage": false,
      "pluginVersion": "9.2.4",
      "pointradius": 2,
      "points": false,
      "renderer": "flot",
      "seriesOverrides": [],
      "spaceLength": 10,
      "stack": false,
      "steppedLine": false,
      "targets": [
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "refCount": 0,
          "refId": "A",
          "target": "aliasByNode(Shelf.SCHROFF.TEMP.*, 3)",
          "textEditor": false
        }
      ],
      "thresholds": [
        {
          "colorMode": "critical",
          "fill": true,
          "line": true,
          "op": "gt",
          "value": 45,
          "yaxis": "left"
        }
      ],
      "timeRegions": [],
      "title": "Schroff Fan Tray Temperatures",
      "tooltip": {
        "shared": true,
        "sort": 0,
        "value_type": "individual"
      },
      "type": "graph",
      "xaxis": {
        "mode": "time",
        "show": true,
        "values": []
      },
      "yaxes": [
        {
          "$$hashKey": "object:297",
          "format": "celsius",
          "logBase": 1,
          "show": true
        },
        {
          "$$hashKey": "object:298",
          "format": "short",
          "logBase": 1,
          "show": true
        }
      ],
      "yaxis": {
        "align": false
      }
    },
    {
      "alert": {
        "alertRuleTags": {},
        "conditions": [
          {
            "evaluator": {
              "params": [
                90
              ],
              "type": "gt"
            },
            "operator": {
              "type": "and"
            },
            "query": {
              "params": [
                "A",
                "5m",
                "now"
              ]
            },
            "reducer": {
              "params": [],
              "type": "avg"
            },
            "type": "query"
          }
        ],
        "executionErrorState": "alerting",
        "for": "35m",
        "frequency": "1m",
        "handler": 1,
        "name": "Schroff Fan Speeds alert",
        "noDataState": "no_data",
        "notifications": [
          {
            "uid": "h_lQ1km7z"
          }
        ]
      },
      "aliasColors": {},
      "annotate": {
        "enable": false
      },
      "bars": false,
      "dashLength": 10,
      "dashes": false,
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "editable": true,
      "fieldConfig": {
        "defaults": {
          "links": []
        },
        "overrides": []
      },
      "fill": 1,
      "fillGradient": 0,
      "grid": {
        "min": 0
      },
      "gridPos": {
        "h": 9,
        "w": 10,
        "x": 10,
        "y": 5
      },
      "hiddenSeries": false,
      "id": 1,
      "legend": {
        "alignAsTable": false,
        "avg": false,
        "current": false,
        "max": false,
        "min": false,
        "rightSide": false,
        "show": true,
        "sideWidth": 100,
        "total": false,
        "values": false
      },
      "lines": true,
      "linewidth": 1,
      "loadingEditor": false,
      "nullPointMode": "null",
      "options": {
        "alertThreshold": true
      },
      "percentage": false,
      "pluginVersion": "9.2.4",
      "pointradius": 5,
      "points": false,
      "renderer": "flot",
      "resolution": 100,
      "scale": 1,
      "seriesOverrides": [],
      "spaceLength": 10,
      "stack": false,
      "steppedLine": false,
      "targets": [
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "refCount": 0,
          "refId": "A",
          "target": "aliasByNode(Shelf.SCHROFF.SPEED.*, 3)"
        }
      ],
      "thresholds": [
        {
          "colorMode": "critical",
          "fill": true,
          "line": true,
          "op": "gt",
          "value": 90
        }
      ],
      "timeRegions": [],
      "title": "Schroff Fan Speeds",
      "tooltip": {
        "query_as_alias": true,
        "shared": false,
        "sort": 0,
        "value_type": "cumulative"
      },
      "type": "graph",
      "xaxis": {
        "mode": "time",
        "show": true,
        "values": []
      },
      "yaxes": [
        {
          "$$hashKey": "object:326",
          "format": "percent",
          "logBase": 1,
          "show": true
        },
        {
          "$$hashKey": "object:327",
          "format": "short",
          "logBase": 1,
          "show": true
        }
      ],
      "yaxis": {
        "align": false
      },
      "zerofill": true
    },
    {
      "collapsed": false,
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "gridPos": {
        "h": 1,
        "w": 24,
        "x": 0,
        "y": 14
      },
      "id": 28,
      "panels": [],
      "targets": [
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "refId": "A"
        }
      ],
      "title": "Comtel Shelf",
      "type": "row"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 13",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 0,
                  "text": "0x9A"
                },
                "1": {
                  "index": 1,
                  "text": "0x9A"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 3,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 76,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "Shelf.COMTEL.FRU.0x9a.Present",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "1",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 11",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x96"
                },
                "1": {
                  "index": 0,
                  "text": "0x96"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 4,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 77,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.FRU.0x96.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "2",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 9",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x92"
                },
                "1": {
                  "index": 0,
                  "text": "0x92"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 5,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 78,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.FRU.0x92.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "3",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 7",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x8e"
                },
                "1": {
                  "index": 0,
                  "text": "0x8e"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 6,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 79,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.FRU.0x8e.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "4",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 5",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x8a"
                },
                "1": {
                  "index": 0,
                  "text": "0x8a"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 7,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 80,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.FRU.0x8a.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "5",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 3",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x86"
                },
                "1": {
                  "index": 0,
                  "text": "0x86"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 8,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 81,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.FRU.0x86.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "6",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 1",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x82"
                },
                "1": {
                  "index": 0,
                  "text": "0x82"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 9,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 82,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.FRU.0x82.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "7",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 2",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x84"
                },
                "1": {
                  "index": 0,
                  "text": "0x84"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 10,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 83,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.FRU.0x84.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "8",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 4",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x88"
                },
                "1": {
                  "index": 0,
                  "text": "0x88"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 11,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 84,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.FRU.0x88.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "9",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 6",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x8c"
                },
                "1": {
                  "index": 0,
                  "text": "0x8c"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 12,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 85,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.FRU.0x8c.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "10",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 8",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x90"
                },
                "1": {
                  "index": 0,
                  "text": "0x90"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 13,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 86,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.FRU.0x90.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "11",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 10",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x94"
                },
                "1": {
                  "index": 0,
                  "text": "0x94"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 14,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 87,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.FRU.0x94.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "12",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 12",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x98"
                },
                "1": {
                  "index": 0,
                  "text": "0x98"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 15,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 88,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.FRU.0x98.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "13",
      "type": "stat"
    },
    {
      "cacheTimeout": "1",
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "description": "Logical: 14",
      "fieldConfig": {
        "defaults": {
          "mappings": [
            {
              "options": {
                "0": {
                  "index": 1,
                  "text": "0x9C"
                },
                "1": {
                  "index": 0,
                  "text": "0x9C"
                }
              },
              "type": "value"
            }
          ],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "red",
                "value": null
              },
              {
                "color": "green",
                "value": 0.5
              }
            ]
          },
          "unit": "short"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 3,
        "w": 1,
        "x": 16,
        "y": 15
      },
      "hideTimeOverride": true,
      "id": 89,
      "links": [],
      "maxDataPoints": 100,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "textMode": "auto"
      },
      "pluginVersion": "9.2.4",
      "repeatDirection": "h",
      "targets": [
        {
          "aggregation": "Last",
          "alias": "$schroff_slot",
          "crit": 1,
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "decimals": 1,
          "disabledValue": "0",
          "displayAliasType": "Always",
          "displayType": "Regular",
          "displayValueWithAlias": "Warning / Critical",
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.FRU.0x9c.Present, 3)",
          "units": "none",
          "valueDisplayRegex": "Present",
          "valueHandler": "Number Threshold",
          "warn": 1
        }
      ],
      "timeFrom": "2m",
      "title": "14",
      "type": "stat"
    },
    {
      "alert": {
        "alertRuleTags": {},
        "conditions": [
          {
            "evaluator": {
              "params": [
                45
              ],
              "type": "gt"
            },
            "operator": {
              "type": "and"
            },
            "query": {
              "params": [
                "A",
                "5m",
                "now"
              ]
            },
            "reducer": {
              "params": [],
              "type": "max"
            },
            "type": "query"
          }
        ],
        "executionErrorState": "alerting",
        "for": "0",
        "frequency": "30s",
        "handler": 1,
        "message": "comtel over-temp",
        "name": "Comtel Fan Tray Temperatures alert",
        "noDataState": "no_data",
        "notifications": [
          {
            "uid": "h_lQ1km7z"
          }
        ]
      },
      "aliasColors": {},
      "bars": false,
      "dashLength": 10,
      "dashes": false,
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "fieldConfig": {
        "defaults": {
          "links": []
        },
        "overrides": []
      },
      "fill": 1,
      "fillGradient": 0,
      "gridPos": {
        "h": 8,
        "w": 10,
        "x": 0,
        "y": 18
      },
      "hiddenSeries": false,
      "id": 4,
      "legend": {
        "alignAsTable": false,
        "avg": false,
        "current": false,
        "max": false,
        "min": false,
        "rightSide": false,
        "show": true,
        "sideWidth": 100,
        "total": false,
        "values": false
      },
      "lines": true,
      "linewidth": 1,
      "nullPointMode": "null",
      "options": {
        "alertThreshold": true
      },
      "percentage": false,
      "pluginVersion": "9.2.4",
      "pointradius": 2,
      "points": false,
      "renderer": "flot",
      "seriesOverrides": [],
      "spaceLength": 10,
      "stack": false,
      "steppedLine": false,
      "targets": [
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "refCount": 0,
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.TEMP.*, 3)",
          "textEditor": false
        }
      ],
      "thresholds": [
        {
          "colorMode": "critical",
          "fill": true,
          "line": true,
          "op": "gt",
          "value": 45
        }
      ],
      "timeRegions": [],
      "title": "Comtel Fan Tray Temperatures",
      "tooltip": {
        "shared": true,
        "sort": 0,
        "value_type": "individual"
      },
      "type": "graph",
      "xaxis": {
        "mode": "time",
        "show": true,
        "values": []
      },
      "yaxes": [
        {
          "$$hashKey": "object:357",
          "format": "celsius",
          "logBase": 1,
          "show": true
        },
        {
          "$$hashKey": "object:358",
          "format": "short",
          "logBase": 1,
          "show": true
        }
      ],
      "yaxis": {
        "align": false
      }
    },
    {
      "alert": {
        "alertRuleTags": {},
        "conditions": [
          {
            "evaluator": {
              "params": [
                95
              ],
              "type": "gt"
            },
            "operator": {
              "type": "and"
            },
            "query": {
              "params": [
                "A",
                "5m",
                "now"
              ]
            },
            "reducer": {
              "params": [],
              "type": "avg"
            },
            "type": "query"
          }
        ],
        "executionErrorState": "alerting",
        "for": "5m",
        "frequency": "1m",
        "handler": 1,
        "name": "Comtel Fan Speeds alert",
        "noDataState": "no_data",
        "notifications": [
          {
            "uid": "h_lQ1km7z"
          }
        ]
      },
      "aliasColors": {},
      "annotate": {
        "enable": false
      },
      "bars": false,
      "dashLength": 10,
      "dashes": false,
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "editable": true,
      "fieldConfig": {
        "defaults": {
          "links": []
        },
        "overrides": []
      },
      "fill": 1,
      "fillGradient": 0,
      "grid": {
        "min": 0
      },
      "gridPos": {
        "h": 8,
        "w": 10,
        "x": 10,
        "y": 18
      },
      "hiddenSeries": false,
      "id": 5,
      "legend": {
        "alignAsTable": false,
        "avg": false,
        "current": false,
        "max": false,
        "min": false,
        "rightSide": false,
        "show": true,
        "sideWidth": 100,
        "total": false,
        "values": false
      },
      "lines": true,
      "linewidth": 1,
      "loadingEditor": false,
      "nullPointMode": "null",
      "options": {
        "alertThreshold": true
      },
      "percentage": false,
      "pluginVersion": "9.2.4",
      "pointradius": 5,
      "points": false,
      "renderer": "flot",
      "resolution": 100,
      "scale": 1,
      "seriesOverrides": [],
      "spaceLength": 10,
      "stack": false,
      "steppedLine": false,
      "targets": [
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "refCount": 0,
          "refId": "A",
          "target": "aliasByNode(Shelf.COMTEL.SPEED.*, 3)"
        }
      ],
      "thresholds": [
        {
          "colorMode": "critical",
          "fill": true,
          "line": true,
          "op": "gt",
          "value": 95
        }
      ],
      "timeRegions": [],
      "title": "Comtel Fan Speeds",
      "tooltip": {
        "query_as_alias": true,
        "shared": false,
        "sort": 0,
        "value_type": "cumulative"
      },
      "type": "graph",
      "xaxis": {
        "mode": "time",
        "show": true,
        "values": []
      },
      "yaxes": [
        {
          "$$hashKey": "object:413",
          "format": "percent",
          "logBase": 1,
          "show": true
        },
        {
          "$$hashKey": "object:414",
          "format": "short",
          "logBase": 1,
          "show": true
        }
      ],
      "yaxis": {
        "align": false
      },
      "zerofill": true
    },
    {
      "collapsed": false,
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "gridPos": {
        "h": 1,
        "w": 24,
        "x": 0,
        "y": 26
      },
      "id": 11,
      "panels": [],
      "targets": [
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "refId": "A"
        }
      ],
      "title": "ServerRoom",
      "type": "row"
    },
    {
      "alert": {
        "alertRuleTags": {},
        "conditions": [
          {
            "evaluator": {
              "params": [
                35
              ],
              "type": "gt"
            },
            "operator": {
              "type": "and"
            },
            "query": {
              "params": [
                "A",
                "5m",
                "now"
              ]
            },
            "reducer": {
              "params": [],
              "type": "max"
            },
            "type": "query"
          }
        ],
        "executionErrorState": "alerting",
        "for": "0",
        "frequency": "30s",
        "handler": 1,
        "message": "Server room temp is above 35C!",
        "name": "Server room temps",
        "noDataState": "no_data",
        "notifications": [
          {
            "uid": "h_lQ1km7z"
          }
        ]
      },
      "aliasColors": {},
      "bars": false,
      "dashLength": 10,
      "dashes": false,
      "datasource": {
        "type": "graphite",
        "uid": "000000002"
      },
      "fieldConfig": {
        "defaults": {
          "links": []
        },
        "overrides": []
      },
      "fill": 1,
      "fillGradient": 0,
      "gridPos": {
        "h": 8,
        "w": 14,
        "x": 3,
        "y": 27
      },
      "hiddenSeries": false,
      "id": 7,
      "legend": {
        "avg": false,
        "current": false,
        "max": false,
        "min": false,
        "show": true,
        "total": false,
        "values": false
      },
      "lines": true,
      "linewidth": 1,
      "nullPointMode": "null",
      "options": {
        "alertThreshold": true
      },
      "percentage": false,
      "pluginVersion": "9.2.4",
      "pointradius": 2,
      "points": false,
      "renderer": "flot",
      "seriesOverrides": [],
      "spaceLength": 10,
      "stack": false,
      "steppedLine": false,
      "targets": [
        {
          "datasource": {
            "type": "graphite",
            "uid": "000000002"
          },
          "refId": "A",
          "target": "server-room.temp1"
        }
      ],
      "thresholds": [
        {
          "colorMode": "critical",
          "fill": true,
          "line": true,
          "op": "gt",
          "value": 35
        }
      ],
      "timeRegions": [],
      "title": "Server room temp",
      "tooltip": {
        "shared": true,
        "sort": 0,
        "value_type": "individual"
      },
      "type": "graph",
      "xaxis": {
        "mode": "time",
        "show": true,
        "values": []
      },
      "yaxes": [
        {
          "$$hashKey": "object:442",
          "format": "celsius",
          "label": "",
          "logBase": 1,
          "show": true
        },
        {
          "$$hashKey": "object:443",
          "format": "short",
          "logBase": 1,
          "show": false
        }
      ],
      "yaxis": {
        "align": false
      }
    }
  ],
  "refresh": "30s",
  "revision": "1.0",
  "schemaVersion": 37,
  "style": "dark",
  "tags": [
    "graphite",
    "carbon"
  ],
  "templating": {
    "list": [
      {
        "allValue": "",
        "current": {
          "selected": false,
          "text": "All",
          "value": "$__all"
        },
        "datasource": {
          "type": "graphite",
          "uid": "000000002"
        },
        "definition": "Shelf.SCHROFF.FRU.*",
        "hide": 0,
        "includeAll": true,
        "label": "schroff slot",
        "multi": false,
        "name": "schroff_slot",
        "options": [],
        "query": "Shelf.SCHROFF.FRU.*",
        "refresh": 2,
        "regex": "/0x8[2-9a-f]|0x9[0-9a-c]/",
        "skipUrlSync": false,
        "sort": 0,
        "tagValuesQuery": "",
        "tagsQuery": "",
        "type": "query",
        "useTags": false
      },
      {
        "current": {
          "selected": false,
          "text": "0x00",
          "value": "0x00"
        },
        "datasource": {
          "type": "graphite",
          "uid": "000000002"
        },
        "definition": "Shelf.COMTEL.FRU.*",
        "hide": 0,
        "includeAll": false,
        "multi": false,
        "name": "comtel_slot",
        "options": [],
        "query": "Shelf.COMTEL.FRU.*",
        "refresh": 1,
        "regex": "",
        "skipUrlSync": false,
        "sort": 0,
        "tagValuesQuery": "",
        "tagsQuery": "",
        "type": "query",
        "useTags": false
      }
    ]
  },
  "time": {
    "from": "now-3h",
    "to": "now"
  },
  "timepicker": {
    "collapse": false,
    "enable": true,
    "notice": false,
    "now": true,
    "refresh_intervals": [
      "5s",
      "10s",
      "30s",
      "1m",
      "5m",
      "15m",
      "30m",
      "1h",
      "2h",
      "1d"
    ],
    "status": "Stable",
    "time_options": [
      "5m",
      "15m",
      "1h",
      "6h",
      "12h",
      "24h",
      "2d",
      "7d",
      "30d"
    ],
    "type": "timepicker"
  },
  "timezone": "browser",
  "title": "ATCA Shelves",
  "uid": "TWjb6eUZk",
  "version": 125,
  "weekStart": ""
}