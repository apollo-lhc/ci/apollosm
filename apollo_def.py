import argparse
import os
import socket
import yaml
import ipmc_def
import pickle
import difflib

TEST_TO_CHECK = {
    """
    Maps the name of the test to the check
    """
    'apollo_number':'apollo.ipmc_board_number_check()',
    'apollo_slot': 'apollo.ipmc_slot_check()',
    'apollo_sensors': 'apollo.sensor_check()',
    'apollo_read_write': 'apollo.read_write_check(out, \'PL_MEM.SCRATCH.WORD_00:\')',
    'apollo_systemctl_failed': 'apollo.sys_ctl_check(output)',
}


class APOLLO:
    """
    Object representing an Apollo module.
    This class is used to perform a variety of unit tests.
    It holds information on the associated IPMC provided the IPMC has been tested
    """

    def __init__(self):
       pass

    def build(self, host,cmd_output):
        """
        Builds the Apollo object from test output. 
        Attributes are empty by default so that the object can be built after every test.
        """
        self.ip = host
        self.name = None
        self.ipmc = None
        self.git_hash = {}
        self.dna = {}
        self.ipmb_address = None
        self.sensors = {}
  
        ipmc_ip = parse_out(cmd_output,'SLAVE_I2C.S8.IPMC_IP:')

        try:
            self.ipmc = get_ipmc(ipmc_ip)

        except FileNotFoundError:
            pass

        self.git_hash = parse_registers(cmd_output,'SM_INFO.GIT_')
        self.dna = parse_registers(cmd_output,'SM_INFO.DNA.')   
        self.parse_sensor(cmd_output,'PL_MEM.SM_POWER.')
        self.parse_sensor(cmd_output,'SLAVE_I2C.S7.PIM.')
        self.name = parse_out_hex(cmd_output,'SLAVE_I2C.S1.SM.INFO.SN:')
        self.ipmb_address = parse_out(cmd_output,'SLAVE_I2C.S1.SM.INFO.SLOT:')

    def print_out(self):
        try:
            print("IP:", self.ip)
            print(f"Name: SM{self.name}")
            print("IPMB address:",self.ipmb_address)
            print("SM FW ver:", self.git_hash['HASH_5']['VALUE'])
            print("IPMC:")
        except:
            pass
        if self.ipmc != None:
            self.ipmc.print_out()
        else:
            print('None')
    
    def parse_sensor(self,cmd_output,field):
        """
        Given the test output and a field, the function outputs a dictionary of all the lines in the test output with that field.
        """
        
        out = ''
        #get the ouput from the sensors 
        if cmd_output != None:
            #narrow down the extraction to sensor output
            lines = cmd_output.split("\n")
            for line1 in lines:
                if field in line1:
                    out = out + f'\n' + line1.split(field)[1].strip()
                    line = ''
            
            lines = out.split("\n")
            i = 0
            for line in lines:
                #ignore first line because it contains nothing
                if i!= 0:

                    data = line.strip().split(':')
                    name = data[0].strip()
                    try:
                        value = float(data[1].strip())
                    except:
                        value = data[1].strip()

                    # Store the data in the dictionary
                    self.sensors[name] = {'VALUE' : value} 
                i = i + 1; 
    
    def ipmc_board_number_check(self):
        """
        Returns false if the ipmc board number does not match the sm board number
        """
        try:
            print('executing ipmc_board_number_check')
            if self.ipmc.board == self.name:
                print("ipmc_board_number_check success")
                return True
            else:
                print("ipmc_board_number_check failed")
                return False
        except:
            #If the ipmc is not found, then the test is skipped
            print('pass')
            pass

    def ipmc_slot_check(self):
        """
        Returns false if ipmc slot address does not match the sm slot address
        """
        try:
            if self.ipmc.ipmb_0_address == self.ipmb_address:
                return True
            else:
                return False
        except:
            pass

    def sensor_check(self):
        """
        Returns the name of the sensor if has a value of 0
        """
        for name, data in self.sensors.items():
            value = data['VALUE']
            if value == 0:
                return name
            
        return None

    def read_write_check(self,cmd_output,field):
        """
        Returns false if the read/write test failed
        """
        scratch = parse_out_rep(cmd_output,field)
        print(scratch)
        try:
            if scratch[0] == scratch[1]:
                return False
            else:
                return True
        except:
            pass
        
    def sys_ctl_check(self,cmd_output):
        """
        Returns false if the sys_ctl_check test failed
        """

        args = parse_cli()
        standard = read_file(args.sys_ctl_path)

        d = difflib.Differ()
        result = ''.join(d.compare(standard,cmd_output))
        
        #+ ● indicates a new item, so an extra ● means that another daemon has failed
        if '+ ●' in result:
            return False
        pass
    
    def make_artifact(self,host,output):
        """
        Making artifact from output
        """
        if self.name != None:
            id = self.name
        else:
            id = host

        with open(f'apollo{id}.out','w') as f:
            f.write(output)
            f.write(str(self.__dict__))
            f.close()


def parse_cli():
    """
    Parses CLI for apollo test.
    Can take either IP or board number.
    """

    parser = argparse.ArgumentParser()

    #parser can take either board number or ip
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-b','--board_number', type=str, help='The serial number of the Apollo SM.',nargs='+')
    group.add_argument('-ip','--apollo_ip',type=str,help='IP address of apollo',nargs='+')

    parser.add_argument('-c', '--config-path', default='config/apollo_ip_config.yaml', help='Path to the Apollo ip config file.')
    parser.add_argument('-ct', '--config-path-test', default='config/apollo_test_config.yaml', help='Path to the Apollo test config file.')
    parser.add_argument('-sys', '--sys_ctl_path', default='config/apollo_systemctl_failed.txt', help='Path to the sys ctl compared output ')
    parser.add_argument('-o', '--out_path', default='artifact/', help='Path to the out file.')


    args = parser.parse_args()
    return args

#if register is read once
def parse_out(cmd_output,field):
    """
    Parses one output from a field in the test output
    """
    out = None

    if cmd_output != None:
        lines = cmd_output.split("\n")
        for line in lines:
            if field in line:
                out = line.split(field)[1].strip()

    return out

#if register is read multiple times
def parse_out_rep(cmd_output,field):
    """
    Parses repeated outputs into a list from a field in the test output
    """
    
    out = []

    if cmd_output != None:
        lines = cmd_output.split("\n")
        for line in lines:
            if field in line:
                out.append(line.split(field)[1].strip())

    return out

#hex to int to string
def parse_out_hex(cmd_output,field):
    """
    Parses one hex into string from test output
    """
    out = None

    if cmd_output != None:
        lines = cmd_output.split("\n")
        for line in lines:
            if field in line:
                out = str(int(line.split(field)[1].strip(),16))

    return out

def parse_registers(cmd_output,field):
    """
    Parses a set of registers into a dict 
    """
    out = ''
    registers = {}

    #get the ouput from the register 
    if cmd_output != None:
        #narrow down the extraction to output
        lines = cmd_output.split("\n")
        for line1 in lines:
            if field in line1:
                out = out + f'\n' + line1.split(field)[1].strip()
                line = ''
        
        lines = out.split("\n")
        i = 0
        for line in lines:
            #ignore first line because it contains nothing
            if i!= 0:

                data = line.strip().split(':')
                name = data[0].strip()
                try:
                    value = float(data[1].strip())
                except:
                    value = data[1].strip()

                # Store the data in the dictionary
                registers[name] = {'VALUE' : value} 
            i = i + 1; 
    return registers


def get_commands():
    """
    Gets commands from test config file
    """
    args = parse_cli()

    apollo_tests = dict(read_config(args.config_path_test))
    cmd_list  = [[key] + val for key, val in apollo_tests.items()]
    
    return cmd_list
    
def read_config(filepath: str):
    """Reads the YAML configuration file from the given file path."""
    if not os.path.exists(filepath):
        raise FileNotFoundError(f"Could not find IPMC configuration file: {filepath}")
    
    with open(filepath, 'r') as f:
        data = yaml.safe_load(f)
    
    return data 

def read_file(filepath: str):
    """Reads file from the given file path."""
    if not os.path.exists(filepath):
        raise FileNotFoundError(f"Could not find sys_ctl output file: {filepath}")
    
    with open(filepath,'r') as f:
        data = f.read()

    return data

def validate_connections():
    """Makes sure the IP is in the config"""
    
    args = parse_cli()

    APOLLO_IP = read_config(args.config_path)
    
    ip_list = list(APOLLO_IP.values())
    host_list = []

    #Goes through all IPs in the list, ignores if invalid
    if args.apollo_ip:
        for i in args.apollo_ip:
            try:
                if i not in ip_list:
                    raise ValueError()
            except:
                print(f'Apollo cannot be found for IP: {i}')
            else:
                host_list.append(f'{i}')

    elif args.board_number:
        for i in args.board_number:
            try:
                if i not in APOLLO_IP:
                    raise ValueError()
            except:
                print(f'Apollo cannot be found for IP: {i}')
            else:
                host_list.append(APOLLO_IP[i])
    else:
        raise ValueError('No Argument')

    return host_list, args.out_path

def get_ipmc(ipmc_ip):
    """Loads ipmc object file"""
    with open(f'{ipmc_ip}.ipmc', 'rb') as ipmc_file:
        ipmc = pickle.load(ipmc_file)
    
    return ipmc


def extract_apollo(host,cmd_output):
    """Constructing apollo object from ssh output using apollo.build """
    apollo = APOLLO()
    apollo.build(host,cmd_output)
    try:
        if int(apollo.sensors['STATUS.ENABLE_A']['VALUE'],16) == 0:
            apollo.sensors.pop('VIN_A')
        else:
            apollo.sensors.pop('VIN_B')
    except:
        pass

    return apollo