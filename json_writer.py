import json
import os
import yaml
import ipmc_def

SHELF = {
    '192.168.10.171' : 'COMTEL',
    '192.168.10.172' : 'SCHROFF'
}
curr_dir = os.path.dirname(__file__)
PANEL_PATH = f'{curr_dir}/panels'

def generate_panel_json(shelf, ipmb_slot, logical_slot, ipmc_id,panel_id):
    """
    Makes json for grafana from yaml config
    """
          
    panel_data = {
        "id": 62,
        "gridPos": {
            "h": 4,
            "w": 1,
            "x": 3,
            "y": 1
        },
        "type": "stat",
        "title": "1",
        "repeatDirection": "h",
        "datasource": {
            "uid": "000000002",
            "type": "graphite"
        },
        "pluginVersion": "9.2.4",
        "timeFrom": "2m",
        "hideTimeOverride": True,
        "maxDataPoints": 100,
        "description": f"Logical {logical_slot}",
        "links": [],
        "cacheTimeout": "1",
        "fieldConfig": {
            "defaults": {
            "mappings": [],
            "thresholds": {
                "mode": "absolute",
                "steps": [
                {
                    "color": "red",
                    "value": None
                },
                {
                    "color": "green",
                    "value": 0.5
                }
                ]
            },
            "unit": "short",
            "color": {
                "mode": "continuous-GrYlRd"
            }
            },
            "overrides": [
            {
                "matcher": {
                "id": "byName",
                "options": f"Shelf.{shelf}.FRU.{ipmb_slot}.Present"
                },
                "properties": [
                {
                    "id": "color",
                    "value": {
                    "mode": "continuous-RdYlGr"
                    }
                }
                ]
            },
            {
                "matcher": {
                "id": "byName",
                "options": f"Shelf.{shelf}.FRU.{ipmb_slot}.Present"
                },
                "properties": [
                {
                    "id": "mappings",
                    "value": [
                    {
                        "type": "value",
                        "options": {
                        "0": {
                            "text": "0x9A",
                            "index": 0
                        },
                        "1": {
                            "text": "0x9A",
                            "index": 1
                        }
                        }
                    }
                    ]
                }
                ]
            }
            ]
        },
        "options": {
            "reduceOptions": {
            "values": False,
            "calcs": [
                "lastNotNull"
            ],
            "fields": ""
            },
            "orientation": "horizontal",
            "textMode": "value",
            "colorMode": "value",
            "graphMode": "none",
            "justifyMode": "center"
        },
        "targets": [
            {
            "aggregation": "Last",
            "alias": "$schroff_slot",
            "crit": 1,
            "datasource": {
                "type": "graphite",
                "uid": "000000002"
            },
            "decimals": 1,
            "disabledValue": "0",
            "displayAliasType": "Always",
            "displayType": "Regular",
            "displayValueWithAlias": "Warning / Critical",
            "refCount": 0,
            "refId": "A",
            "target": f"Shelf.{shelf}.FRU.{ipmb_slot}.Present",
            "units": "none",
            "valueDisplayRegex": "Present",
            "valueHandler": "Number Threshold",
            "warn": 1
            },
            {
            "datasource": {
                "type": "graphite",
                "uid": "000000002"
            },
            "hide": False,
            "refCount": 0,
            "refId": "B",
            "target": f"Shelf.Slot.{ipmc_id}.IPMC.SN.ID"
            },
            {
            "aggregation": "Last",
            "alias": "$schroff_slot",
            "crit": 1,
            "datasource": {
                "type": "graphite",
                "uid": "000000002"
            },
            "decimals": 1,
            "disabledValue": "0",
            "displayAliasType": "Always",
            "displayType": "Regular",
            "displayValueWithAlias": "Warning / Critical",
            "refCount": 0,
            "refId": "C",
            "target": f"isNonNull(Shelf.{shelf}.FRU.{ipmb_slot}.SENSORS.*)",
            "units": "none",
            "valueDisplayRegex": "Present",
            "valueHandler": "Number Threshold",
            "warn": 1,
            "hide": False
            }
        ]
        }
    return panel_data

def generate_json(config):
    dashboard = {
            "annotations": {
        "enable": False,
        "list": [
        {
            "builtIn": 1,
            "datasource": {
            "type": "datasource",
            "uid": "grafana"
            },
            "enable": True,
            "hide": True,
            "iconColor": "rgba(0, 211, 255, 1)",
            "name": "Annotations & Alerts",
            "target": {
            "limit": 100,
            "matchAny": False,
            "tags": [],
            "type": "dashboard"
            },
            "type": "dashboard"
        }
        ]
    },
    "editable": True,
    "fiscalYearStartMonth": 0,
    "graphTooltip": 0,
    "id": 1,
    "links": [],
    "liveNow": False,
    "panels": []
    }
    panel_id = 10

    filename_list = []

    for shelf_ip, slot in config['Shelves'].items():
        shelf = SHELF[shelf_ip]
        for ipmb_slot, value in slot.items():
            
            try:
                logical_slot = value['Logical Slot']
            except:
                logical_slot = -1
            
            try:
                ipmc_id = value['IPMC ID']            
            except:
                ipmc_id = -1

            panel_data = generate_panel_json(shelf, ipmb_slot, logical_slot, ipmc_id,panel_id)
            panel_id = panel_id + 1
            dashboard['panels'].append(panel_data)
            
        for filename in os.listdir(PANEL_PATH):
            print(f"Testing contains {str.lower(filename).__contains__(str.lower(f'{shelf}'))}")
            if filename.endswith(".json") and str.lower(filename).__contains__(str.lower(f"{shelf}")):
                filename_list.append(filename)
                file_path = os.path.join(PANEL_PATH, filename)
                with open(file_path, 'r') as file:
                    print(f"Loading {filename} to {shelf}")
                    file_panels = json.load(file)
                    dashboard['panels'].append(file_panels)
                


    for filename in os.listdir(PANEL_PATH):
        if filename.endswith(".json") and filename not in filename_list:
            file_path = os.path.join(PANEL_PATH, filename)
            with open(file_path, 'r') as file:
                print(f"Appending {filename} at end")
                file_panels = json.load(file)
                dashboard['panels'].append(file_panels)

    filename = "dashboard.json"
    with open(filename, 'w') as file:
        json.dump(dashboard, file, indent=4)

    print(f"Created JSON file: {filename}")
  


    
   