import subprocess
import time
import apollo_def

def main():
   

    [host_list, out_path] = apollo_def.validate_connections()

    

    apollo_list = []

    #Connects to each apollo and sends a set of commands
    for HOST in host_list:
        cmd_list = apollo_def.get_commands()
        output = ""
        error = ""
        test_list = []
        no_connecion = False
        no_key = False

        for cmd_set in cmd_list:
            out = ''
            #First item in cmd_set is the name of the test
            test = cmd_set[0]
            print('\nExecuting',test)
            del cmd_set[0]

            try:

                ssh_cmd = ['ssh', f'cms@{HOST}']
                
                ssh_session = subprocess.Popen(ssh_cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

                #Writing command by command
                for cmd in cmd_set:
                    ssh_session.stdin.write(cmd.encode())
                    ssh_session.stdin.write(b'\n')
                ssh_session.stdin.close()
                
                #adding each line to output with a new line
                for line in ssh_session.stdout:
                    l = line.decode().rstrip()
                    #print(l)
                    out = out + f'\n' + l

                for line in ssh_session.stderr:
                    err = line.decode().rstrip()
                    #print('Error:',err)
                    error = error + f'\n' + err
                    
                    #raise flag if no connection
                    if 'No route to host' in err:
                        no_connecion = True
                        break
                    if 'Host key verification failed.' in err:
                        no_key = True
                        break

                ssh_session.wait()

                if no_connecion == True:
                    print(f'No connection, ignoring {HOST}')
                    break 
                elif no_key == True:
                    print(f'No key, ignoring {HOST}')
                    break

                if out == '':
                    print(f'Error: no output {error}')
                    break
                    
                #move on to next apollo if no connection
                 
                        
            except subprocess.TimeoutExpired:
                print("SSH connection timed out.")
            except Exception as e:
                print("An error occurred:", e)
            
            #construct apollo from only output of one test
            apollo = apollo_def.extract_apollo(HOST,out)
            output = output + out
            test_list.append(test)

            #Cancels all remaining tests if check fails, and moves on to next apollo
            if test in apollo_def.TEST_TO_CHECK:
                print(f'eval {apollo_def.TEST_TO_CHECK[test]}')
                check = eval(apollo_def.TEST_TO_CHECK[test])
                if check != True and check != None :
                    print(f'{HOST} failed {test} (check: {check} = 0), moving to next apollo')
                    break
                else:
                    print(f'{HOST} passed {test}')
            #print(apollo.__dict__)

        #construct apollo object from output of tests
        apollo = apollo_def.extract_apollo(HOST,output)
        apollo_list.append(apollo)
        apollo.ipmc_board_number_check()
        apollo.print_out()
        """ for test in test_list:
            if test in apollo_def.TEST_TO_CHECK:
                check = eval(apollo_def.TEST_TO_CHECK[test])
                if check != True and check != None :
                    print(f'{HOST} failed {test}')         """
        
        #making artifact 
        with open(f'apollo{HOST}.out','w') as f:
            f.write(output)
            f.write(f"Error: {error}\n")
            f.write(str(apollo.__dict__))
            f.close()

        #print(apollo.__dict__)
        
 
if __name__ == '__main__':   
    main()
