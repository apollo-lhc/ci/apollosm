import subprocess
import time
import apollo_def

def main():

    cmd_list = apollo_def.get_commands()

    #To get ip
    cmd_list.insert(0,['apollo_get_ip','hostname -I','exit'])

    #Connects to each apollo and sends a set of commands
    test_list = []
    output = ""
    error = ""
    
    for cmd_set in cmd_list:
        #First item in cmd_set is the name of the test
        out = ""
        test = cmd_set[0]
        print('\nExecuting',test)
        del cmd_set[0]

        try:

            #setting the first command in the set to the start command
            start_cmd = cmd_set[0]
            del cmd_set[0]
            test_session = subprocess.Popen(start_cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,shell=True)

            #Writing command by command
            for cmd in cmd_set:
                test_session.stdin.write((cmd + '\n').encode())
                #test_session.stdin.write(b'\n')
                test_session.stdin.flush()
            test_session.stdin.close()
            
            #adding each line to output with a new line
            for line in test_session.stdout:
                l = line.decode().rstrip()
                out = out + f'\n' + l

            for line in test_session.stderr:
                err = line.decode().rstrip()
                error = error + f'\n' + err     

            test_session.wait()

        except subprocess.TimeoutExpired:
            print("SSH connection timed out.")
        except Exception as e:
            print("An error occurred:", e)
        
        apollo = apollo_def.extract_apollo(None,out)
        output = output + out
        test_list.append(test)

        #Cancels all remaining tests if check fails, and moves on to next apollo
        if test in apollo_def.TEST_TO_CHECK:
            check = eval(apollo_def.TEST_TO_CHECK[test])
            if check != True and check != None :
                print(f'Failed {test}, moving to next apollo')
                break
            else:
                print(f'Passed {test}')

    #construct apollo object from output
    apollo = apollo_def.extract_apollo(None,output)
    apollo.print_out()

    #making artifact 
    with open(f'apollo.out','w') as f:
        f.write(output)
        f.write(str(apollo.__dict__))
        f.close()

if __name__ == '__main__':   
    main()