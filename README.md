# ApolloSM

## Name
Apollo Continuous Integration tests

## Description
Provides tests that verifies that the apollo blade functions properly, which includes the IPMC, service module, and command module. Uses ssh or telnet to communicate with machines from another machine in the network. The tests can also be executed on the apollo service module using local_apollo.py. Tests for the service and command module are written in apollo_test_config.yaml. 
Checks are implemented for:
- read/write 
- system control
- apollo sensors
- ipmc firmware between ipmi tool and telnet

### Shelf Monitor Daemon
Periodically checks if sensors for each ipmb slot on the shelves are above non-critical (nc), critical (cr), or non-recoverable (nr) values. These values are then pushed to grafana. It also generates a json for grafana.

## Badges

## Visuals

## Build/Installation
```
git clone https://gitlab.com/apollo-lhc/ci/apollosm.git
```

## How to Use
```
test_ipmc.py -ip [ipmc ip address] 
test_apollo.py -ip [apollo ip address]
```
Or:
```
test_ipmc.py -b [apollo board number]
test_apollo.py -b [apollo board number]
```
The IPs and board numbers can be changed in the config.
Use local_apollo.py when running tests locally. 
Run test_ipmc.py first so that it generates object that can linked to the associated apollo board.

### Daemon
```
python3 shelf_monitor.py -c {config file path} -l {log file path} &
```
Make sure to put the service file in the right spot. There is also sensor_daemon.py that also runs checks on ipmcs on the shelf. 
## Output

Expected output at this stage:
```
$ python3 test_apollo.py -b 208

Executing apollo_connection

Executing apollo_ipmc_ip

Executing apollo_slot

Executing apollo_number

Executing apollo_git_hash

Executing apollo_read_write
['0x00000000', '0x0000000F']
192.168.34.208 passed apollo_read_write

Executing apollo_dna

Executing apollo_systemctl_failed
192.168.34.208 passed apollo_systemctl_failed

Executing apollo_cm_test

Executing apollo_sensors
192.168.34.208 passed apollo_sensors
IP: 192.168.34.208
Name: 208
IPMB address: 0x8E
SM FW ver: 0x09C39AD5
IPMC:
IP: 192.168.22.41
IPMB-0 Address: 0x8e
ipmitool fru time: 736.675ms
Firmware Commit: 592091e8
```
## Roadmap
- [x] Running the apollo tests locally
- [ ] Adding more tests
- [ ] Adding a pass/fail condition for all of the tests
- [X] Pushing IPMC sensors status to grafana
- [x] shelf monitor daemon pushing to grafana
- [ ] Configure Grafana