#!/usr/bin/env python3

import os
import sys
import time
import yaml
import socket
import argparse

# Check Python version, need at least 3.6
valid_python = sys.version_info.major >= 3 and sys.version_info.minor >= 6 
assert valid_python, "You need Python version >=3.6 to run this script!"

SHELF = {
    '192.168.10.171' : 'COMTEL',
    '192.168.10.172' : 'SCHROFF'
}

#  
class SLOT:
    """Generic class to represent something connected to an ipmb address on the shelves"""
    def __init__(self):
        pass

    def build(self,file_contents,ipmb_address,shelf_ip,config):
        """Makes slot object given information"""
        self.grafana_config = config['graphite']
        self.shelf = shelf_ip
        self.shelf_name = SHELF[shelf_ip]
        self.ipmb_0_address = ipmb_address
        self.sensors = None
        self.unhappy_sensors = []
        self.ABOVE_NC = []
        self.ABOVE_CR = []
        self.ABOVE_NR = []
        #for comparison with updated sensor readings
        self.ABOVE_NC_length = 0
        self.ABOVE_NR_length = 0
        self.ABOVE_CR_length = 0

        self.fru_info = []    

    def parse_sensors(self,file_contents):
        """Parses sensors that are not ok into a list"""
        if not file_contents:
            print(f'No sensors on slot {self.ipmb_0_address}')
            pass
        else:
            if "Get Device ID command failed" in file_contents:
                print(f'Could not connect to self.ipmb_0_address')
            elif "Hot Swap" in file_contents:
                split = file_contents.split("Hot Swap ")
                #removes empty lines
                lines = [line for line in split[1].split('\n') if line.strip()]
            else:
                split = file_contents
                lines = [line for line in split.split('\n') if line.strip()]

            output = [line.strip().split('|') for line in lines]
            #delete last element because empty
            #del self.sensors[-1]

            #removes non sensors
            self.sensors = []
            self.unhappy_sensors = []
            for row in output:
                sensor_status = row[3].strip()
                if not (sensor_status.startswith("0x") or sensor_status.startswith("0X") or sensor_status == 'na'):
                    self.sensors.append(row)

            for row in self.sensors:
                sensor_name = row[0].strip()
                sensor_value = row[1].strip()
                upper_threshold = row[8].strip()
                sensor_status = row[3].strip()

                if sensor_value != 'na' and upper_threshold != 'na' and float(sensor_value) > float(upper_threshold):
                    print(f"WARNING: {sensor_name} has a value of {sensor_value}, which exceeds its upper threshold of {upper_threshold}")

                if sensor_status not in ['ok']:
                    print(f'WARNING: {sensor_name} has status {sensor_status}')
                    self.unhappy_sensors.append(row)

    
    def sort_sensors(self):
        """Sorts not ok sensors into three categories"""
        self.ABOVE_NC = []
        self.ABOVE_CR = []
        self.ABOVE_NR = []

        for row in self.unhappy_sensors:
            sensor_status = row[3].strip()
            sensor_name = row[0].strip()

            if 'nc' in sensor_status:
                self.ABOVE_NC.append(row)
                print(f"Adding {sensor_name} to ABOVE_NC list {self.ABOVE_NC_length} ")      
            elif 'cr' in sensor_status:
                self.ABOVE_CR.append(row)
            elif 'nr' in sensor_status:
                self.ABOVE_NR.append(row)               
            else:
                print(f'{sensor_name} in unknown state {sensor_status}')         
    
    def check_sensors(self):
        """Returns false if at least one sensor is not ok"""
        
        #check for changes in sensor readings
        if (len(self.ABOVE_NC) != self.ABOVE_NC_length) or (len(self.ABOVE_CR) != self.ABOVE_NC_length) or ( len(self.ABOVE_NR) != self.ABOVE_NR_length):
            self.ABOVE_CR_length = len(self.ABOVE_CR)
            self.ABOVE_NC_length = len(self.ABOVE_NC)
            self.ABOVE_NR_length = len(self.ABOVE_NR)
            self.push_grafana()
        
        if len(self.unhappy_sensors):
            return False
        else:
            return True
        
    def convert_graphite_strings(self):
        """Turn the entry into graphite recognizable strings"""

        timestamp = int(time.time())
        
        result = [] 
        result.append(f"Shelf.{self.shelf_name}.FRU.{self.ipmb_0_address}.SENSORS.ABOVE_NC {len(self.ABOVE_NC)} {timestamp}\n")
        result.append(f"Shelf.{self.shelf_name}.FRU.{self.ipmb_0_address}.SENSORS.ABOVE_CR {len(self.ABOVE_CR)} {timestamp}\n")
        result.append(f"Shelf.{self.shelf_name}.FRU.{self.ipmb_0_address}.SENSORS.ABOVE_NR {len(self.ABOVE_NR)} {timestamp}\n")
        print(result)
        return result
    
    def parse_fru(self,file_contents):
        """Parses FRU information from telnet"""
        if file_contents:
            lines = file_contents.split("\n")
            for line in lines:
                self.fru_info.append(line)
        else:
            print(f'No FRU information on slot {self.ipmb_0_address}')

    def check_fru(self):
        """Returns false if FRU information is not present"""
        if "FRU Device Description" in str(self.fru_info[0]) and str(self.fru_info[1]) :
            return True
        return False

    def push_grafana(self):
        """Pushes graphite strings to database"""
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            print(self.grafana_config)
            server = self.grafana_config['server']
            
            strings = self.convert_graphite_strings()
            print("pushing to grafana")
            try:
                s.connect((server["ip"], server["port"]))
                
                # Encode the data with utf-8 and send it to the socket
                for string in strings:
                    s.sendall(bytes(string, 'utf-8')) 
                    #print(string)
                # Provided IP address cannot be found
                # Warn the user that we cannot push to Graphite
            except socket.error:
                print(f'Cannot reach Graphite server at IP: {server["ip"]}, port: {server["port"]}')

def extract_slot(file_contents,ipmb_address,shelf_ip,config):
    """makes ipmc object from information from telnet"""
    slot = SLOT()
    slot.build(file_contents,ipmb_address,shelf_ip,config)

    return slot