import sys
if sys.version_info < (3,6):
  raise BaseException("Wrong Python version detected. Please ensure that you are using Python >=3.6!")
import sched
import signal
import pidfile
import daemon
import subprocess
import socket
import time
import argparse
import logging
from logging.handlers import RotatingFileHandler
import ipmc_def
import slot_def
import json_writer

PIDFILE_PATH = '/var/run/sensor_daemon.pid'             # PID File Path (Configured in service file)
#LOGFILE_PATH = '/var/log/sensor_daemon.log'             # Log File Path
LOGFILE_PATH = '/opt/shelf_monitor/apollosm/sensor_daemon.log'
CONFIG_FILE_PATH = '/etc/sensor_daemon'

running = True

#formatter = logging.Formatter('%(asctime)s %(message)s', datefmt='%d-%b-%y %H:%M:%S')

#Redirects prints from other python files to log to avoid printing on cmd line
class PrintToLogger:
    def write(self, message):
        logging.info(message)

    def flush(self):
        pass


DEFAULT_CONFIG = {
    'Shelves': {
        '192.168.10.172' : {
            '0x9a': {'IPMC IP Address': '192.168.22.32','Board': 207},
            '0x8e': {'IPMC IP Address': '192.168.22.41', 'Board': 208},
            '0x94': {'IPMC IP Address': '192.168.22.42', 'Board': 209},
            '0x8c': {'IPMC IP Address': '192.168.22.36', 'Board': 211},
            '0x86': {'IPMC IP Address': '192.168.22.3', 'Board': 212},
        },
       '192.168.10.171':{
            '0x96': {'IPMC IP Address': '192.168.21.4', 'Board': 215},
            '0x90': {'IPMC IP Address': '192.168.22.39', 'Board': 216},
        },
    },
    'graphite' : {
        'push': 'true',
        'server' : {
                'ip': '192.168.10.20', 
                'port': 2003
        }
    },
    'wait_time':600
}

SHELF = {
    '192.168.10.171' : 'COMTEL',
    '192.168.10.172' : 'SCHROFF'
}

def load_config(filepath: str):
    """Loads config"""
    try:
        data = ipmc_def.read_config(filepath)
        logging.error(f"Config loaded from {filepath}")

    except Exception as e:
        logging.error(f"An error occurred: {e}")
        # If config not found, use default
        data = DEFAULT_CONFIG
    
    config = data  
    
    json_writer.generate_json(config)

    return config

def test_shelf(config):
    """Connects to every ipmb slot in every shelf provided in the config"""
    timeout = 5
    ipmc_list = []
    shelf_ip = ''

    #loops over every shelf (only comtel and shroff at the moment)
    for shelf_ip, slot in config['Shelves'].items():
        logging.info("Shelf IP: %s",shelf_ip)
        #slot is dictonary that may contain the device's IP and associated apolloSM board if it has one
        logging.debug(slot)

        #loops over every device on shelf
        for ipmb_slot, value in slot.items():
            HOST = ''
            
            try:
                HOST = value['IPMC IP Address']
            except:
                logging.warning(f'No IP for device {ipmb_slot}')

            try:
                try:
                    if HOST:
                        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                            # Make the connection
                            s.connect((HOST, ipmc_def.PORT))
                            s.settimeout(timeout)
                            
                            logging.debug(f'Connection established to: {HOST}:{ipmc_def.PORT}')
                            logging.debug(f'Timeout set to: {timeout} s')

                            # Execute the commands and read back data
                            try:
                                output = ipmc_def.write_command_and_read_output(s, "info\r\n")
                                logging.debug("INFO: %s",output)
                                
                            except socket.timeout:
                                logging.warning('-> Command timed out, skipping.')
                            
                            time.sleep(0.5)
                            
                            # Do a final read of the EEPROM before exiting
                            out = ipmc_def.write_command_and_read_output(s, "eepromrd\r\n")
                            logging.debug('EEPROM read: %s',out)
                    
                        logs = out+output

                        #Extracting information and making ipmc object
                        slot_object = ipmc_def.extract_ipmc(logs)                    
                        ipmc_list.append(slot_object)
                        
                except Exception as e:
                    logging.error(f"Error: {HOST}: {e}")

                try:
                    if slot_object.ipmb_0_address:
                        ipmb_addr = slot_object.ipmb_0_address
                    else:
                        ipmb_addr = ipmb_slot
                except:
                    slot_object = ''
                    ipmb_addr = ipmb_slot
                    

                #Uses shelf designation from config to connect to appropriate shelf IP
                fru_cmd = f"ipmitool -H {shelf_ip} -P \"\" -t {ipmb_addr} fru > fru_logs_slot"
                logging.info(fru_cmd)
                subprocess.run(fru_cmd,shell=True)

                logging.debug("Fru Logs: \n%s",ipmc_def.read_logs('fru_logs_slot'))

                if not slot_object:
                    slot_object = slot_def.extract_slot(ipmc_def.read_logs('fru_logs_slot'),ipmb_addr,shelf_ip,config)

                slot_object.parse_fru(ipmc_def.read_logs('fru_logs_slot'))
                fru_check = slot_object.check_fru()
                logging.info("Fru Check: %s", fru_check)

                #checking sensors
                if fru_check:
                    sensor_cmd = f"ipmitool -H {shelf_ip} -P \"\" -t {ipmb_addr} sensor > sensor_logs_slot"
                    logging.info(sensor_cmd)
                    subprocess.run(sensor_cmd,shell=True)
                    
                    logging.debug("Sensor Logs: \n%s",ipmc_def.read_logs('sensor_logs_slot'))

                    slot_object.parse_sensors(ipmc_def.read_logs('sensor_logs_slot'))
                    
                    if slot_object.sensors:
                        logging.debug("Sensors: %s",slot_object.sensors)
                        logging.info("Sensor Check: %s", slot_object.check_sensors())
                        try :
                            slot_object.push_grafana()
                        except Exception as e:
                            slot_object.push_grafana_i(config["graphite"],shelf_ip)
                    
                #Object is deleted to avoid existing in next loop iteration
                del slot_object

            except Exception as e:
                logging.error(f"Error: {ipmb_slot}: {e}")

def func_daemon(config, dryrun):
    global running

    logging.info("Execution started.")

    def signal_handler(signum, frame):  
        """Custom signal handler. Stops running the daemon once the SIGINT signal is recieved."""
        global running
        logging.info(f"Handler got {str(signum)}")
        logging.info(f"Running is {str(running)}")
        running = False
    
    signal.signal(signal.SIGINT, signal_handler)
    
    test_shelf(config)

    if dryrun:
        logging.info("Dry run requested, exiting.")
        sys.exit(0)

    wait_time = config['wait_time']

    # Make sure that it runs every ten minutes
    while running:
        time.sleep(wait_time)
        test_shelf(config)
    
    # SIGINT signal received, shut down
    logging.info("Shutdown")
    sys.exit(0)

def main ():
    parser = argparse.ArgumentParser()
    parser.add_argument("--nodaemon", help="Don't daemonize this process", action="store_true")
    parser.add_argument("--dry", help="Dry run, will only run once", action="store_true")
    parser.add_argument('-c', '--config_path', default=CONFIG_FILE_PATH, help='Path to the IPMC IP and graphite config file.')
    parser.add_argument('-l', '--logfile_path', default=LOGFILE_PATH, help='Path to the daemon log.')
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")

    args = parser.parse_args()     

    # file_handler = LOGGER.FileHandler(args.logfile_path, 'a')
    # file_handler.setFormatter(formatter)
    # LOGGER.addHandler(file_handler)
    # LOGGER.setLevel(LOGGER.INFO)

    

    # if args.verbose:
    #     LOGGER.setLevel(LOGGER.DEBUG)
    #     LOGGER.debug("Verbose mode on")

    #LOGGER.debug(args.config_path)
    config = load_config(args.config_path)

    #no daemon mode
    if args.dry:
        dry = True
    else:
        dry = False

    logging_format = "[%(asctime)s] %(levelname)-8s %(message)s"


    if args.nodaemon:
        logging.basicConfig(level=logging.INFO, format=logging_format,handlers=[RotatingFileHandler(LOGFILE_PATH, backupCount=2, maxBytes=10 * 1024**2)])
        sys.stdout = PrintToLogger()  
        if args.verbose:
            logging.setLevel(logging.DEBUG)
            logging.debug("Verbose mode on")
                    
        logging.info("Running daemon mode")
        logging.debug(config)
        func_daemon(config,dry)     

    else:
        #LOGGER.info("Running Daemon mode (outside context)")

        with daemon.DaemonContext(working_directory="/tmp/", pidfile=pidfile.PIDFile(PIDFILE_PATH)) as context:
            logging.basicConfig(level=logging.INFO, format=logging_format,handlers=
                                [RotatingFileHandler(LOGFILE_PATH, backupCount=2, maxBytes=10 * 1024**2)])
            sys.stdout = PrintToLogger()  
            if args.verbose:
                logging.setLevel(logging.DEBUG)
                logging.debug("Verbose mode on")
                        
            logging.info("Running daemon mode")
            logging.debug(config)
            func_daemon(config,dry)


if __name__ == '__main__':   
    main()
