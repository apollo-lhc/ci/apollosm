import sys
if sys.version_info < (3,6):
  raise BaseException("Wrong Python version detected. Please ensure that you are using Python >=3.6!")
import sched
import signal
import pidfile
import daemon
import subprocess
import socket
import time
import argparse
import logging
from logging.handlers import RotatingFileHandler
import ipmc_def
import slot_def
import json_writer


#PIDFILE_PATH = '/tmp/sensor_daemon.pid'
PIDFILE_PATH = '/var/run/shelf_monitor.pid'             # PID File Path (Configured in service file)
#LOGFILE_PATH = '/var/log/shelf_monitor.log'             # Log File Path
LOGFILE_PATH = '/opt/shelf_monitor/apollosm/shelf_monitor.log'
CONFIG_FILE_PATH = '/etc/shelf_monitor'

running = True

slot_list = []
#formatter = logging.Formatter('%(asctime)s %(message)s', datefmt='%d-%b-%y %H:%M:%S')

#Redirects prints from other python files to log to avoid printing on cmd line
class PrintToLogger:
     
     def __init__(self, LOGGER):
         self.LOGGER = LOGGER

     def write(self, message):
         self.LOGGER.info(message)

     def flush(self):
         pass

     def flush(self):
         pass


DEFAULT_CONFIG = {
    'Shelves': {
        '192.168.10.172' : {
            '0x9a': {'IPMC IP Address': '192.168.22.32','Board': 207},
            '0x8e': {'IPMC IP Address': '192.168.22.41', 'Board': 208},
            '0x94': {'IPMC IP Address': '192.168.22.42', 'Board': 209},
            '0x8c': {'IPMC IP Address': '192.168.22.36', 'Board': 211},
            '0x86': {'IPMC IP Address': '192.168.22.3', 'Board': 212},
        },
       '192.168.10.171':{
            '0x96': {'IPMC IP Address': '192.168.21.4', 'Board': 215},
            '0x90': {'IPMC IP Address': '192.168.22.39', 'Board': 216},
        },
    },
    'graphite' : {
        'push': 'true',
        'server' : {
                'ip': '192.168.10.20', 
                'port': 2003
        }
    },
    'wait_time':600
}

SHELF = {
    '192.168.10.171' : 'COMTEL',
    '192.168.10.172' : 'SCHROFF'
}

def load_config(filepath: str,LOGGER):
    """Loads config"""
    data = DEFAULT_CONFIG
    try:
        data = ipmc_def.read_config(filepath)
        LOGGER.error(f"Config loaded from {filepath}")

    except Exception as e:
        LOGGER.error(f"An error occurred: {e}")
        # If config not found, use default
        data = DEFAULT_CONFIG
    
    config = data  
    
    json_writer.generate_json(config)

    return config

def generate_slot_list(config):
    for shelf_ip, slot in config['Shelves'].items():
        for ipmb_slot, value in slot.items():
            slot_object = slot_def.extract_slot('',ipmb_slot,shelf_ip,config)
            slot_list.append(slot_object)

def test_shelf(LOGGER):
    """Connects to every ipmb slot in every shelf provided in the config"""   
    #loops over every shelf (only comtel and shroff at the moment)
    for slot in slot_list:
        try:            
            #checking sensors
            sensor_cmd = f"ipmitool -H {slot.shelf} -P \"\" -t {slot.ipmb_0_address} sensor"
            LOGGER.info(sensor_cmd)
            test_session = subprocess.Popen(sensor_cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,shell=True)
            out = ''
            error = ''
            for line in test_session.stdout:
                l = line.decode().rstrip()
                out = out + f'\n' + l

            for line in test_session.stderr:
                err = line.decode().rstrip()
                error = error + f'\n' + err                  
            if error:
                LOGGER.error('ipmitool error: %s',error)
            LOGGER.debug("Sensor Logs: \n%s",out)

            slot.parse_sensors(out)
            slot.sort_sensors()
            
            if slot.sensors:
                LOGGER.debug("Sensors: %s",slot.sensors)
                LOGGER.info("Sensor Check: %s", slot.check_sensors())                
        
        except Exception as e:
            LOGGER.error(f"Error: {slot.ipmb_0_address}: {e}")

def push_to_grafana():
    """Pushes the all sensor data to the graphite server"""
    for slot in slot_list:
        if slot.sensors:
            slot.push_grafana()

def func_daemon(config, dryrun,LOGGER):
    global running

    LOGGER.info("Execution started.")

    def signal_handler(signum, frame):  
        """Custom signal handler. Stops running the daemon once the SIGINT signal is recieved."""
        global running
        LOGGER.info(f"Handler got {str(signum)}")
        LOGGER.info(f"Running is {str(running)}")
        running = False
    
    signal.signal(signal.SIGINT, signal_handler)
    generate_slot_list(config)
    test_shelf(LOGGER)
    push_to_grafana() 

    if dryrun:
        LOGGER.info("Dry run requested, exiting.")
        sys.exit(0)

    wait_time = config['wait_time']

    # Make sure that it runs every ten minutes
    last_push_time = time.time() 

    while running:
        test_shelf(LOGGER)
        current_time = time.time()

        elapsed_time = current_time - last_push_time
        if elapsed_time >= wait_time:  
            push_to_grafana() 
            last_push_time = current_time
    
    # SIGINT signal received, shut down
    LOGGER.info("Shutdown")
    sys.exit(0)

def main ():
    parser = argparse.ArgumentParser()
    parser.add_argument("--nodaemon", help="Don't daemonize this process", action="store_true")
    parser.add_argument("--dry", help="Dry run, will only run once", action="store_true")
    parser.add_argument('-c', '--config_path', default=CONFIG_FILE_PATH, help='Path to the IPMC IP and graphite config file.')
    parser.add_argument('-l', '--logfile_path', default=LOGFILE_PATH, help='Path to the daemon log.')
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")

    args = parser.parse_args()     

    # file_handler = LOGGER.FileHandler(args.logfile_path, 'a')
    # file_handler.setFormatter(formatter)
    # LOGGER.addHandler(file_handler)
    # LOGGER.setLevel(LOGGER.INFO)

    

    # if args.verbose:
    #     LOGGER.setLevel(LOGGER.DEBUG)
    #     LOGGER.debug("Verbose mode on")

    #LOGGER.debug(args.config_path)
    #config = load_config(args.config_path)

    #no daemon mode
    if args.dry:
        dry = True
    else:
        dry = False

    logging_format = "[%(asctime)s] %(levelname)-8s %(message)s"


    if args.nodaemon:
        LOGGER = logging.getLogger('Shelf_Monitor')
        file_handler = logging.FileHandler(args.logfile_path, 'a')
        formatter = logging.Formatter('%(asctime)s: %(name)s: %(levelname)s: %(message)s')
        file_handler.setFormatter(formatter)
        LOGGER.addHandler(file_handler)
        LOGGER.setLevel(logging.INFO)

        sys.stdout = PrintToLogger(LOGGER)  
        if args.verbose:
            LOGGER.setLevel(logging.DEBUG)
            LOGGER.debug("Verbose mode on")
                    
        LOGGER.info("Running no daemon mode")
        config = load_config(args.config_path,LOGGER)
        LOGGER.debug(config)
        func_daemon(config,dry,LOGGER)     

    else:
        #LOGGER.info("Running Daemon mode (outside context)")
       
        with daemon.DaemonContext(working_directory="/tmp/", pidfile=pidfile.PIDFile(PIDFILE_PATH)) as context:
            LOGGER = logging.getLogger('Shelf_Monitor')
            file_handler = logging.FileHandler(args.logfile_path, 'a')
            formatter = logging.Formatter('%(asctime)s: %(name)s: %(levelname)s: %(message)s')
            file_handler.setFormatter(formatter)
            LOGGER.addHandler(file_handler)
            LOGGER.setLevel(logging.INFO)

            sys.stdout = PrintToLogger(LOGGER)  
            if args.verbose:
                LOGGER.setLevel(logging.DEBUG)
                LOGGER.debug("Verbose mode on")
                        
            LOGGER.info("Running daemon mode")
            config = load_config(args.config_path,LOGGER)
            LOGGER.debug(config)
            func_daemon(config,dry,LOGGER)  


if __name__ == '__main__':   
    main()
