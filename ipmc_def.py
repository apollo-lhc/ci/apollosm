#!/usr/bin/env python3

import os
import sys
import time
import yaml
import socket
import argparse
import slot_def

# Check Python version, need at least 3.6
valid_python = sys.version_info.major >= 3 and sys.version_info.minor >= 6 
assert valid_python, "You need Python version >=3.6 to run this script!"

# The port for the telnet service on the IPMC
PORT = 23

# GRAPHITE_CONFIG = {
#     'push': 'true',
#     'server' : {
#             'ip': '192.168.10.20', 
#             'port': 2003
#     }

# }

#define IPMC object with fields gathered from telnetting to it and ipmi tool
class IPMC (slot_def.SLOT):

    def build(self,file_contents):
        #makes ipmc object from information from telnet
        lines = file_contents.split("\n")
        self.hw = None
        self.ip = None
        self.ipmb_0_address = None
        self.firmware_commit = None
        self.board = None
        self.ipmi_time = None
        self.sensors = None
        self.fru_info = []
        self.unhappy_sensors = []
        self.ABOVE_NC = []
        self.ABOVE_CR = []
        self.ABOVE_NR = []
        #for comparison with updated sensor readings
        self.ABOVE_NC_length = 0
        self.ABOVE_NR_length = 0
        self.ABOVE_CR_length = 0    

        for line in lines:
            if "IP Addr:" in line:
                self.ip = line.split("IP Addr:")[1].strip()
            elif "IPMB-0 Addr:" in line:
                self.ipmb_0_address = line.split("IPMB-0 Addr:")[1].strip()
            elif "IPMB-0 Address:" in line:
                self.ipmb_0_address = line.split("IPMB-0 Address:")[1].strip()
            elif "Firmware commit:" in line:
                self.firmware_commit = line.split("Firmware commit:")[1].strip()
            elif "hw           =" in line:
                self.hw = line.split("hw           =")[1].strip()
        
        self.board = self.hw.split('#')[1].strip()

    def check_firmware(self,file_contents):
        """Checks if the telnet firmware version is the same as the one given by ipmitool"""
        lines = file_contents.split("\n")
        firmware = None
        for line in lines:
            if "Product Asset Tag     :" in line:
                firmware = line.split("Product Asset Tag     :")[1].strip()
        e = firmware.lower()
        f = self.firmware_commit
        if e == f:
            return True
        else:
            return False

    def parse_sensors(self,file_contents):
        """Parses sensors that not ok into a list"""
        if not file_contents:
            print(f'No device on slot {self.ipmb_0_address}')
            pass
        else:
            try:
                split = file_contents.split("Hot Swap ")
                lines = split[1].split('\n')
                self.sensors = [line.strip().split('|') for line in lines]
                
                #delete last element because empty
                del self.sensors[-1]

                try:
                    for row in self.sensors:
                        #print(row)
                        sensor_name = row[0].strip()
                        sensor_value = row[1].strip()
                        upper_threshold = row[8].strip()
                        sensor_status = row[3].strip()
                        if sensor_name in ['Carrier','IPMB-0 Sensor']:
                            continue
                        if sensor_value != 'na' and upper_threshold != 'na' and float(sensor_value) > float(upper_threshold):
                            print(f"WARNING: {sensor_name} has a value of {sensor_value}, which exceeds its upper threshold of {upper_threshold}")
                        
                        if sensor_status not in ['ok','na']:
                            print(f'WARNING: {sensor_name} has status {row[3]}')
                            #group unhappy sensors into three noncritical (nc), critical (cr), non recoverable (nr)
                            self.unhappy_sensors.append(row)
                except:
                    pass
            except:
                print(file_contents)
                pass

    def print_out(self):
        print('IP:', self.ip)
        print('HW:', self.hw)
        print('IPMB-0 Address:', self.ipmb_0_address)
        print(f"ipmitool fru time: {self.ipmi_time:.03f}ms")
        print('Firmware Commit:', self.firmware_commit)
        
    def convert_graphite_strings_i(self):
        # Turn the entry into graphite recognizable strings
        timestamp = int(time.time())
        
        result = [] 
        result.append(f"Apollo{self.board}.IPMC.SENSORS.ABOVE_NC {len(self.ABOVE_NC)} {timestamp}\n")
        result.append(f"Apollo{self.board}.IPMC.SENSORS.ABOVE_CR {len(self.ABOVE_CR)} {timestamp}\n")
        result.append(f"Apollo{self.board}.IPMC.SENSORS.ABOVE_NR {len(self.ABOVE_NR)} {timestamp}\n")

        return result
    def push_grafana_i(self,config,shelf):

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            
            server = config['server']
            
            strings = self.convert_graphite_strings_i()
            try:
                s.connect((server["ip"], server["port"]))
                
                # Encode the data with utf-8 and send it to the socket
                for string in strings:
                    s.sendall(bytes(string, 'utf-8')) 
                    #print(string)
                # Provided IP address cannot be found
                # Warn the user that we cannot push to Graphite
            except socket.error:
                print(f'Cannot reach Graphite server at IP: {server["ip"]}, port: {server["port"]}')

    #for yaml config
    def to_dict(self):
        return {
            'hw': self.hw,
            'Ipmi Time':self.ipmi_time,
            'IPMB 0 address': self.ipmb_0_address,
            'Firmware commit': self.firmware_commit,
            'IP': self.ip,
        }
    

def parse_cli():
    parser = argparse.ArgumentParser()
    #either board number or ip needs to be provided
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-b','--board_number', type=str, help='The serial number of the Apollo SM.',nargs='+')
    group.add_argument('-ip','--ipmc_ip',type=str,help='IP address of IPMC',nargs='+')

    parser.add_argument('-c', '--config_path', default='config/ipmc_ip_config.yaml', help='Path to the IPMC IP config file.')
    parser.add_argument('-o', '--out_path', default='ipmc_out.yaml', help='Path to the IPMC out file.')

    args = parser.parse_args()
    return args


def read_config(filepath: str):
    """Reads the YAML configuration file from the given file path."""
    if not os.path.exists(filepath):
        raise FileNotFoundError(f"Could not find IPMC configuration file: {filepath}")
    
    with open(filepath) as parameters:
        data = yaml.safe_load(parameters)
    
    return data

def write_command_and_read_output(
    sock: socket.socket, 
    command: str,
    max_size: int=2048,
    read_until: bytes=b">",
    ) -> str:
    """
    Given the socket interface, writes a command to IPMC TelNet interface 
    and returns the data echoed back.
    The maximum message size to expect is specified via the max_size argument.
    """
    counter = 0
    data = b""

    # Send the command one byte at a time
    for ch in command:
        sock.send(bytes(ch, 'utf-8'))
        # Read back the command
        _ = sock.recv(1)

    # Recieve the echoed message one byte at a time
    while counter < max_size:
        rcv = sock.recv(1)
        if rcv == read_until:
            if counter != 0:
                break
            else:
                continue
        data += rcv
        counter += 1

    # Get the leftover ">" and " " characters before exiting
    for i in range(2):
        _ = sock.recv(1)

    return data.decode('ascii')

def validate_connections():
    #Makes sure the IP is in the config

    args = parse_cli()

    SM_TO_IPMC = read_config(args.config_path)


    ipmc_ip_list = []
    board_list = []

    
    if args.ipmc_ip:
        for i in args.ipmc_ip:
            try:
                if i not in SM_TO_IPMC:
                    raise ValueError(f'IPMC cannot be found for IP: {i}')
            except:
                print(f' cannot be found for IP: {i}')
            else:
                ipmc_ip_list.append(f'{i}')

    elif args.board_number:
        for i in args.board_number:
            try:
                if i not in SM_TO_IPMC:
                    raise ValueError(f'IPMC cannot be found for Apollo: {i}')
            except:    
                print(f'IPMC cannot be found for Apollo: {i}')
            else:
                board_list.append(f'{i}')
    else:
        raise ValueError('No Argument')

    return ipmc_ip_list, board_list, args.out_path


def extract_ipmc(file_contents):
    #makes ipmc object from information from telnet
    ipmc = IPMC()
    ipmc.build(file_contents)

    return ipmc

def read_logs(file_path):
    with open(file_path) as f:
        file_contents = f.read()
    return file_contents

def check_firmware(file_contents,IPMC):
    #Checks if the telnet firmware version is the same as the one given by ipmitool
    lines = file_contents.split("\n")
    check = None
    firmware = None
    for line in lines:
        if "Product Asset Tag     :" in line:
            firmware = line.split("Product Asset Tag     :")[1].strip()

    if firmware.lower() == IPMC.getFirmware():
        check = True
    else:
        check = False
    return check

def write_ipmc_to_yaml(ipmc_list, filepath):
    with open(filepath, 'w') as f:
        yaml.dump([ipmc.to_dict() for ipmc in ipmc_list], f)
